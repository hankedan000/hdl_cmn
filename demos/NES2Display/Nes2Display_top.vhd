----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:26:08 12/04/2016 
-- Design Name: 
-- Module Name:    Nes2Display_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Nes2Display_top is
    Port ( CLK 		: in	STD_LOGIC;
           CLR 		: in	STD_LOGIC;
			  
           CTRL_DAT 	: in	STD_LOGIC;
           CTRL_LAT 	: out	STD_LOGIC;
           CTRL_CLK 	: out	STD_LOGIC;
			  
           AN 			: out	STD_LOGIC_VECTOR (7 downto 0);
           SEGMENT 	: out	STD_LOGIC_VECTOR (6 downto 0));
end Nes2Display_top;

architecture Behavioral of Nes2Display_top is
	component Clock_Divided_Counter is
		generic (n			:integer	:=8;
					timer_width:integer	:=8);
		Port (	CNT	:	in		STD_LOGIC_VECTOR (timer_width-1 downto 0);
					CLR	:	in		STD_LOGIC;
					EN		:	in		STD_LOGIC;
					CLK	:	in		STD_LOGIC;
						
					Q		:	out	STD_LOGIC_VECTOR (n-1 downto 0));
	end component;

	component NES_Controller_Reader is
		Port ( 	CTRL_CLK	:	out	STD_LOGIC;
					CTRL_LAT	:	out	STD_LOGIC;
					CTRL_DAT	:	in		STD_LOGIC;
					
					CLK		:	in		STD_LOGIC;
					CLR		:	in		STD_LOGIC;
					DATA_OUT	:	out	STD_LOGIC_VECTOR	(7 downto 0));
	end component;
	
	component Numeric_Display is
		 Port ( 	NUM		:	in		STD_LOGIC_VECTOR	(31 downto 0);
					MASK		:	in		STD_LOGIC_VECTOR	(7 downto 0);
					EN			:	in		STD_LOGIC;
					CLK		:	in		STD_LOGIC;
				
					SEGMENT	:	out	STD_LOGIC_VECTOR	(6 downto 0);
					AN			:	out	STD_LOGIC_VECTOR	(7 downto 0));
	end component;
	
	-- Intermediate signals
	signal ctrl_data 		: STD_LOGIC_VECTOR(7 downto 0);
	signal clk_1000Hz_cnt: STD_LOGIC_VECTOR(0 downto 0);
	signal clk_1000Hz		: STD_LOGIC;
begin
	READER : NES_Controller_Reader port map(
		CTRL_CLK => CTRL_CLK,
		CTRL_LAT => CTRL_LAT,
		CTRL_DAT => CTRL_DAT,
		
		CLK		=> clk_1000Hz,
		CLR		=> CLR,
		DATA_OUT	=> ctrl_data
	);
	
	CLK_GEN_1000HZ : Clock_Divided_Counter generic map(n=>1,timer_width=>20) port map(
		CNT	=> x"186A0", -- 100,000 in hex
		CLR	=> CLR,
		EN		=> '1',
		CLK	=> CLK,
		
		Q		=> clk_1000Hz_cnt
	);
	clk_1000Hz <= clk_1000Hz_cnt(0);
	
	DISPLAY : Numeric_Display port map(
		NUM		=> x"000000" & ctrl_data,
		MASK		=> "00000011",
		EN			=> '1',
		CLK		=> CLK,
	
		SEGMENT	=> SEGMENT,
		AN			=> AN
	);

end Behavioral;

