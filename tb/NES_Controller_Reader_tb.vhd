----------------------------------------------------------------------------
-- Entity:        NES_Controller_Reader_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Reg
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--   Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

----------------------------------------------------------------------------
entity NES_Controller_Reader_tb is
end    NES_Controller_Reader_tb;
----------------------------------------------------------------------------

architecture Behavioral of NES_Controller_Reader_tb is

	-- Unit Under Test (UUT)
	component NES_Controller_Reader is
		Port ( 	CTRL_CLK	:	out	STD_LOGIC;
					CTRL_LAT	:	out	STD_LOGIC;
					CTRL_DAT	:	in		STD_LOGIC;
					
					CLK		:	in		STD_LOGIC;
					CLR		:	in		STD_LOGIC;
					DATA_OUT	:	out	STD_LOGIC_VECTOR	(7 downto 0));
	end component;

   --Inputs
   signal CTRL_DAT	: STD_LOGIC;
	signal CLK	    	: STD_LOGIC	:= '0';
	signal CLR			: STD_LOGIC := '1';

 	--Outputs
	signal CTRL_CLK	: STD_LOGIC;
	signal CTRL_LAT	: STD_LOGIC;
   signal DATA_OUT	: STD_LOGIC_VECTOR	(7 downto 0);
	
	-- Internals signals
	signal byte_to_send	: STD_LOGIC_VECTOR(7 downto 0);
	subtype TEST_WORD is std_logic_vector(7 downto 0);
	type TEST_TABLE is array(0 to 3) of TEST_WORD;
	constant test_vector: TEST_TABLE := TEST_TABLE'(
		x"DE",	-- 0x0000
		x"AD",	-- 0x0001
		x"BE",	-- 0x0002
		x"EF");	-- 0x0003
begin

	-- Instantiate the Unit Under Test (UUT)
   CTRL_READER: NES_Controller_Reader port map (CTRL_CLK,CTRL_LAT,CTRL_DAT,CLK,CLR,DATA_OUT);

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 40 ns.
      wait for 30 ns;
		CLR <= '0';
		
		for w in 0 to 3 loop
			byte_to_send <= test_vector(w);
			
			wait until rising_edge(CTRL_LAT);
			CTRL_DAT <= byte_to_send(0);
			
			for b in 1 to 7 loop
				wait until rising_edge(CTRL_CLK);
				CTRL_DAT <= byte_to_send(b);
			end loop;
		end loop;
		
		wait; -- end sim
   end process;
	
	-- Clock
	CLK <= not CLK after 10 ns;

end Behavioral;

