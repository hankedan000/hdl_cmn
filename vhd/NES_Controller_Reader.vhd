----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:22:46 12/03/2016 
-- Design Name: 
-- Module Name:    NES_Controller_Reader - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity NES_Controller_Reader is
	Port ( 	CTRL_CLK	:	out	STD_LOGIC;
				CTRL_LAT	:	out	STD_LOGIC;
				CTRL_DAT	:	in		STD_LOGIC;
				
				CLK		:	in		STD_LOGIC;
				CLR		:	in		STD_LOGIC;
				DATA_OUT	:	out	STD_LOGIC_VECTOR	(7 downto 0));
end NES_Controller_Reader;

architecture Behavioral of NES_Controller_Reader is
	Component Counter is
	generic (n:integer:=8);
		 Port ( 	CLR	:	in		STD_LOGIC;
					EN		:	in		STD_LOGIC;
					CLK	:	in		STD_LOGIC;
					
					Q		:	out	STD_LOGIC_VECTOR (n-1 downto 0));
	end Component;
	
	-- Intermediate signals
	signal int_count	: STD_LOGIC_VECTOR(2 downto 0);
	signal int_clk		: STD_LOGIC;
	signal int_lat		: STD_LOGIC;
	signal int_data	: STD_LOGIC_VECTOR(7 downto 0);
begin
	COUNT:	Counter generic map(n => 3) port map(
		CLR		=> CLR,
		EN			=>	'1',
		CLK		=> CLK,
		
		Q			=> int_count
	);
	
	int_lat	<=	CLK	when (int_count = "000") else
					'0';
					
	int_clk	<=	CLK	when (unsigned(int_count) > 0) else
					'0';
	
	process (CLK,CLR) is
	begin
		if falling_edge(CLK) then
			if ((int_clk = '1') or (int_lat = '1')) then
				int_data <= CTRL_DAT & int_data(7 downto 1);
			end if;
		end if;
		
		if rising_edge(CLK) then
			if (int_count = "111") then
				DATA_OUT <= int_data;
			end if;
		end if;
		
		if (CLR = '1') then
			int_data <= x"00";
		end if;
	end process;

	CTRL_LAT <= int_lat;
	CTRL_CLK <= int_clk;
	
end Behavioral;

